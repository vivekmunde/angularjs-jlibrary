﻿(function (jModalModule) {
    "use strict";
    jModalModule.directive("jModal", ["$parse", "$modal",
    function ($parse, $modal) {
        var jModal = {
            restrict: "EA",
            link: function (scope, elm, attrs) {
                var modalState = 0;
                var modal = null;
                var open = function () {
                    if (modalState != "O") {
                        modalState = "O";
                        modal = $modal.open({
                            templateUrl: attrs.jModalTemplateUrl,
                            scope: scope,
                            backdrop: 'static',
                            windowClass: attrs.jModalClass
                        });
                        modal.opened.then(function () {
                            modalState = "O";
                            if (attrs.jModalOnOpen) {
                                $parse(attrs.jModalOnOpen)(scope);
                            };
                        });
                        modal.result.then(function (args) {
                            modalState = "C";
                            if (attrs.jModalOnClose) {
                                $parse(attrs.jModalOnClose)(scope);
                            };
                        }, function (args) {
                            modalState = "D";
                            if (attrs.jModalOnClose) {
                                $parse(attrs.jModalOnClose)(scope);
                            };
                        });
                    };
                };
                if (attrs.jModalOpenOn) {
                    scope.$on(attrs.jModalOpenOn, function () {
                        arguments[0].preventDefault();
                        open();
                    });
                };
                if (attrs.jModalCloseOn) {
                    scope.$on(attrs.jModalCloseOn, function () {
                        arguments[0].preventDefault();
                        modal.close();
                    });
                };
            }
        };
        return jModal;
    }]);
})(angular.module("jModalModule", []));
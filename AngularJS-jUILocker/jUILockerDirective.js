﻿(function (jUILockerModule) {
    "use strict";
    jUILockerModule.directive("jAppLocker", ["$modal",
    function ($modal) {
        var locker = {
            restrict: "EA",
            link: function (scope, elm, attrs) {
                var appProcesses = 0;
                var modal = null;
                var open = function () {
                    if (appProcesses <= 0) {
                        appProcesses++;
                        modal = $modal.open({
                            templateUrl: attrs.jAppLockerTemplateUrl,
                            scope: scope,
                            backdrop: 'static',
                            windowClass: attrs.jAppLockerClass
                        });
                        modal.result.then(function () {
                            appProcesses = 0;
                        }, function () {
                            appProcesses = 0;
                        });
                    };
                };
                var close = function () {
                    if (appProcesses <= 0) {
                        modal.close();
                    };
                };
                if (attrs.jAppLockerLockOn) {
                    scope.$on(attrs.jAppLockerLockOn, function (e) {
                        e.preventDefault();
                        open();
                    });
                };
                if (attrs.jAppLockerUnlockOn) {
                    scope.$on(attrs.jAppLockerUnlockOn, function (e) {
                        appProcesses--;
                        e.preventDefault();
                        close();
                    });
                };
            }
        };
        return locker;
    }]);
})(angular.module("jUILockerModule", []));
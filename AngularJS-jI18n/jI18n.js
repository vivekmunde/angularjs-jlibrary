﻿(function (jI18nModule) {
    "use strict";
    jI18nModule.factory("jI18nService", ["$http", "$rootScope", "$window", "$filter",
    function ($http, $rootScope, $window, $filter) {
        var i18n = {

            /* The service loads the resource text from server and fires event so that the listener can detect the change. */

            /* Default options. 
            the options can be modofied before initiating the load. */
            options: {
                broadcast: true /* Flag to broadcast the event */,
                event: "jI18nUpdate" /* Event name to broacast */
            },
            /* Dictionary to hold the resource text collection. */
            dictionary: [],
            onSuccess: function () { },
            onError: function () { },
            then: function thenI18n() {
                i18n.onSuccess = arguments[0];
                i18n.onError = arguments[1];
            },
            load: function loadI18n(url) {
                /* Get the resource text collection. */
                $http({
                    method: "GET", url: url, cache: false
                }).success(function loadI18nSuccess(data, status, headers, config) {
                    i18n.dictionary = data;
                    /* Broadcast the update. */
                    if (i18n.options.broadcast) {
                        $rootScope.$broadcast(i18n.options.event);
                    };
                    i18n.onSuccess(data);
                }).error(function loadI18nError(error, status, headers, config) {
                    i18n.onError(error);
                });
                return this;
            },
            /* Function to get the specific resource tet for the provided key. */
            get: function getI18n(key) {
                var result = "";
                if ((i18n.dictionary !== []) && (i18n.dictionary.length > 0)) {
                    var entry = $filter("filter")(i18n.dictionary,
                        function (element) {
                            return element.key === key;
                        }
                    )[0];
                    if (entry != undefined && entry != null) {
                        result = entry.value;
                    };
                };
                return result;
            }
        };
        return i18n;
    }]);

    /*===========================================================================================*/

    jI18nModule.directive("jI18n", ["jI18nService",
        function (jI18nService) {
            var jI18n = {
                restrict: "EAC",
                update: function updateI18n(elm, attrs) {

                    /* ## Prerequisites: 
                    The directive is used along with the i18nService 'jI18nService'. 
                    The service is the resource text supplier. */

                    /* jI18n attribute value format = elmAttribute|resourceKey.
                    elmAttribute = Attribute of the element to which the resource text to be applied.
                    resourceKey = Resource identification key. 
                    ## Extend: Modify the format to extend the directive. */
                    if (attrs.jI18n != null && attrs.jI18n != undefined && attrs.jI18n != "") {
                        var attrValueSplit = attrs.jI18n.split("|");
                        /* Get the resource text from service. */
                        var text = jI18nService.get(attrValueSplit[1]);
                        var i18nAttr = attrValueSplit[0];
                        /* Apply the resource text. 
                        ## Extend: Modify the code to extend the directive to support multiple attributes. */
                        if (i18nAttr == "text") {
                            elm.text(text);
                        } else {
                            elm.attr(i18nAttr, text);
                        }
                    };
                },
                link: function linkI18n(scope, elm, attrs) {
                    /* A lister listening to any updates on the resource text. 
                    The event is fired from the i18n service when there is an update in the resource text. */
                    /* Check if element is require to listen to the update. */
                    if (attrs.jI18nListen == "true") {
                        scope.$on(jI18nService.options.event, function () {
                            jI18n.update(elm, attrs);
                        });
                    };
                    /* An observer to detech any change in the elmAttribute or resourceKey  */
                    /* Check if element is require to observe the change in reource key. */
                    if (attrs.jI18nObserve == "true") {
                        attrs.$observe("jI18n", function (value) {
                            jI18n.update(elm, attrs);
                        });
                    } else {
                        jI18n.update(elm, attrs);
                    };
                }
            };

            return jI18n;
        }]);
})(angular.module("jI18nModule", []));
﻿(function (jBooterModule) {
    "use strict";
    jBooterModule.directive("jBooter", ["$compile", "$http", "$location", "$parse",
     function ($compile, $http, $location, $parse) {

         var jBooter = {
             restrict: "EA",
             link: function (scope, elm, attrs) {
                 var requestededPath = null;
                 var bootScope = null;

                 /* The function to start the boot process */
                 var boot = function booterBoot() {
                     /* Get the HTML layout to be displayed during the boot process. 
                     Typical example of HTML can be an image displaying loading symbol. 
                     ## Tip: In case of internalitionalization/globalization, 
                         do not use any text inside the boot HTML as the application is yet to grab the lingual resource text in the boot process. 
                         No sense in displaying an English text when the application is planing to grab (in boot process) and display in French. */
                     $http({
                         url: attrs.jBooterBootTemplateUrl,
                         headers: { "content-type": "application/txt" }
                     }).then(function booterBootSuccess(response) {
                         /* Store the requested path so that this path can be called after the booting is over. */
                         requestededPath = $location.url();
                         /* Generate a runtime path by using time.
                         ## Note: Customize this new path code as per requirement. */
                         $location.path(new Date().getTime());
                         /* Call the registered listerner which is to be called once the boot process starts. */
                         if (attrs.jBooterOnStart) {
                             $parse(attrs.jBooterOnStart)(scope);
                         }
                         /* Inject the HTMl layout in the element. */
                         elm.html(response.data);
                         /* Create a new scope for boot process. */
                         bootScope = scope.$new();
                         /* Compile the HTML with the boot-scope. */
                         $compile(elm.contents())(bootScope);
                     }, function booterBootError(response) {
                         /* Call the registered listerner which is to be called when the boot process fails. */
                         if (attrs.jBooterOnError) {
                             $parse(attrs.jBooterOnError)(scope);
                         }
                     });
                 };

                 /* Register a listener to watch the boot progress. 
                 When the boot progress (bootUpdate) reaches 100% then stop the boot process and redirect to the requested path. 
                 jBooterBootUpdate holds the event name which is fired, along with boot progress value, by the consumer application of this directive to indicate the boot progress. 
                 ## Other approach: Event listener can be replaced by model watcher i.e. $watch(model, ...). */
                 var bootWatcher = scope.$on(attrs.jBooterBootUpdate, function booterBootUpdate(e, bootUpdate) {
                     if (bootUpdate >= 100) {
                         /* Get the HTMl layout of the application to render.
                         Typical example of HTML layout of an application can be like 
                         <header>menu</header> 
                         <section ng-view></section>
                         <footer><footer> 
                         */
                         $http({
                             url: attrs.jBooterAppTemplateUrl,
                             headers: { "content-type": "application/txt" }
                         }).then(function booterCompleteSuccess(response) {
                             /* Remove the boot HTML layout. 
                             Here the directive's HTML is removed from from the DOM. 
                             ## Note: Modify the code to remove just the HTML layout of the boot process, if required. */
                             var elmParent = $(elm[0].parentElement);
                             elm.remove();
                             /* Destroy the boot scope. */
                             bootScope.$destroy();
                             /* Replace the HTML layout of the application inside the element. 
                             Here the app HTML is injected in the prent element of the directive so that the directive itself is removed from the DOM. 
                             ## Note: Modify the code to inject the app HTML layout inside the boot html, if required. */
                             elmParent.html(response.data);
                             /* Compile the app HTML with the scope. */
                             $compile(elmParent.contents())(scope);
                             /* Call the registered listerner which is to be called once the boot process is complete. */
                             if (attrs.jBooterOnEnd) {
                                 $parse(attrs.jBooterOnEnd)(scope);
                             }
                             /* Redirect to the requested path. */
                             $location.path(requestededPath);
                             /* Remove the listener as it is no more required to listen to the boot update because the boot process is complete. */
                             bootWatcher();
                             bootWatcher = null;
                         }, function booterCompleteError(response) {
                             /* Call the registered listerner which is to be called when the boot process fails. */
                             if (attrs.jBooterOnError) {
                                 $parse(attrs.jBooterOnError)(scope);
                             }
                         });
                     };
                 });

                 boot();
             }
         };

         return jBooter;
     }]);
})(angular.module("jBooterModule", []));
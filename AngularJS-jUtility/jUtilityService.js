﻿(function (jUtilityModule) {
    "use strict";
    jUtilityModule.factory("idUtility", [
    function () {
        return {
            convertToQueryString: function (data) {
                var convert = function (args) {
                    var queryString = "";
                    angular.forEach(args, function (value, key) {
                        if ($.type(value) === "string" && value != "") {
                            queryString += key + "=" + value + "&";
                        } else if ($.type(value) === "number") {
                            queryString += key + "=" + value + "&";
                        } else if ($.type(value) === "array") {
                            angular.forEach(value, function (arrayKey, arrayValue) {
                                queryString += convert(arrayValue);
                            });
                        } else if ($.type(value) === "object") {
                            queryString += convert(value);
                        };
                    });
                    return queryString;
                };
                return convert(data);
            }
        };
    }]);
})(angular.module("jUtilityModule", []));
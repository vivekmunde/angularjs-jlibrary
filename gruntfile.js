module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        clean: {
            build: ["jLib.js"]
        },
        concat: {
            jLib: {
                src: ["AngularJS-jBooter/BabySteps(V1.0)/jBooterDirective.js",
                    "AngularJS-jI18n/BabySteps(V1.0)/jI18n.js",
                    "AngularJS-jModal/BabySteps(V1.0)/jModalDirective.js",
                    "AngularJS-jUILocker/BabySteps(V1.0)/jUILockerDirective.js",
                    "AngularJS-jUtility/BabySteps(V1.0)/jUtilityService.js"],
                dest: "jLib.js"
            }
        }
    });
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.registerTask("default", ["clean", "concat"]);
};